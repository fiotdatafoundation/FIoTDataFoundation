--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Debian 10.6-1.pgdg90+1)
-- Dumped by pg_dump version 11.3

-- Started on 2019-06-21 02:41:01 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 16385)
-- Name: factory; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA factory;


ALTER SCHEMA factory OWNER TO postgres;

--
-- TOC entry 2877 (class 0 OID 0)
-- Dependencies: 8
-- Name: SCHEMA factory; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA factory IS 'factory data';


--
-- TOC entry 4 (class 2615 OID 16386)
-- Name: settings; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA settings;


ALTER SCHEMA settings OWNER TO postgres;

--
-- TOC entry 2878 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA settings; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA settings IS 'settings';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 16387)
-- Name: account; Type: TABLE; Schema: factory; Owner: postgres
--

CREATE TABLE factory.account (
    token_id text NOT NULL,
    term_id text NOT NULL
);


ALTER TABLE factory.account OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16393)
-- Name: errorinfo; Type: TABLE; Schema: factory; Owner: postgres
--

CREATE TABLE factory.errorinfo (
    equipment_id text NOT NULL,
    error_no smallint NOT NULL,
    error_detail text NOT NULL,
    tweet_text text NOT NULL,
    voicefile_name text NOT NULL,
    term_id text NOT NULL
);


ALTER TABLE factory.errorinfo OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16399)
-- Name: api; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.api (
    name text NOT NULL,
    url text NOT NULL
);


ALTER TABLE settings.api OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16405)
-- Name: broker; Type: TABLE; Schema: settings; Owner: postgres
--

CREATE TABLE settings.broker (
    server_address text NOT NULL,
    voice_path text NOT NULL,
    qos smallint NOT NULL,
    keepalive smallint NOT NULL
);


ALTER TABLE settings.broker OWNER TO postgres;

--
-- TOC entry 2868 (class 0 OID 16387)
-- Dependencies: 198
-- Data for Name: account; Type: TABLE DATA; Schema: factory; Owner: postgres
--

COPY factory.account (token_id, term_id) FROM stdin;
TESTBED01	TESTBED01
\.


--
-- TOC entry 2869 (class 0 OID 16393)
-- Dependencies: 199
-- Data for Name: errorinfo; Type: TABLE DATA; Schema: factory; Owner: postgres
--

COPY factory.errorinfo (equipment_id, error_no, error_detail, tweet_text, voicefile_name, term_id) FROM stdin;
TEST	999	つぶやきテスト	つぶやきテスト	testalarm.wav	TESTBED01
\.


--
-- TOC entry 2870 (class 0 OID 16399)
-- Dependencies: 200
-- Data for Name: api; Type: TABLE DATA; Schema: settings; Owner: postgres
--

COPY settings.api (name, url) FROM stdin;
loginToken	andon/loginToken
postTerminal	andon/postTerminal
checkTerminal	andon/checkTerminal
postTsubuyaki	andon/postTsubuyaki
logout	andon/logout
\.


--
-- TOC entry 2871 (class 0 OID 16405)
-- Dependencies: 201
-- Data for Name: broker; Type: TABLE DATA; Schema: settings; Owner: postgres
--

COPY settings.broker (server_address, voice_path, qos, keepalive) FROM stdin;
http://10.16.6.163:18080/HidTalk/	/var/voicefiles/	1	60
\.


--
-- TOC entry 2742 (class 2606 OID 16412)
-- Name: account account_master_pkey; Type: CONSTRAINT; Schema: factory; Owner: postgres
--

ALTER TABLE ONLY factory.account
    ADD CONSTRAINT account_master_pkey PRIMARY KEY (token_id, term_id);


--
-- TOC entry 2744 (class 2606 OID 16414)
-- Name: errorinfo error_master_pkey; Type: CONSTRAINT; Schema: factory; Owner: postgres
--

ALTER TABLE ONLY factory.errorinfo
    ADD CONSTRAINT error_master_pkey PRIMARY KEY (equipment_id, error_no);


--
-- TOC entry 2746 (class 2606 OID 16416)
-- Name: api api_master_pkey; Type: CONSTRAINT; Schema: settings; Owner: postgres
--

ALTER TABLE ONLY settings.api
    ADD CONSTRAINT api_master_pkey PRIMARY KEY (name, url);


-- Completed on 2019-06-21 02:41:01 UTC

--
-- PostgreSQL database dump complete
--

