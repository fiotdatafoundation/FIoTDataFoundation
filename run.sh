docker network create confluent

#delete node-red container
docker stop $(docker ps -aqf "name=node-red")
docker rm $(docker ps -aqf "name=node-red")

#delete hid-postges container
docker stop $(docker ps -aqf "name=hid-postgres")
docker rm $(docker ps -aqf "name=hid-postgres")

#delete hid-pgadmin container
docker stop $(docker ps -aqf "name=hid-pgadmin")
docker rm $(docker ps -aqf "name=hid-pgadmin")

#delete node-red images
docker rmi $(docker images nodered/node-red-docker -q)

#delete postgres images
docker rmi $(docker images postgres -q)

#delete pgadmin images
docker rmi $(docker images dpage/pgadmin4 -q)

#delete not use images
docker rmi --no-prune=true $(docker images -a -q)

# create docker subnet
docker build -t nodered/node-red-docker:v8 .

# node-red
docker run -d \
    --net=confluent \
    --name=node-red \
    -v $PWD/var/node-red/data:/data \
    -p 1880:1880 \
    --user=root \
    nodered/node-red-docker:v8

docker cp voicefiles $(docker ps -aqf "name=node-red"):/var/voicefiles

#postgreSQL
docker run -d \
    --net=confluent \
    --name=hid-postgres \
    -v $PWD/var/postgres/sql:/docker-entrypoint-initdb.d \
    -v $PWD/var/postgres/data:/var/lib/postgresql/data \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=densoI0T \
    -e POSTGRES_DB=hidtalk \
    -p 5432:5432 \
    --user=root \
    --restart=unless-stopped \
    postgres:10.6

#pgadmin
docker run -d \
    --net=confluent \
    --name=hid-pgadmin \
    -v $PWD/var/pgadmin:/var/lib/pgadmin/storage/postgres \
    -e PGADMIN_DEFAULT_EMAIL=postgres \
    -e PGADMIN_DEFAULT_PASSWORD=densoI0T \
    -p 5050:80 \
    --restart=unless-stopped \
    dpage/pgadmin4
