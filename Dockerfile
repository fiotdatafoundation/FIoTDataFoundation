FROM nodered/node-red-docker:v8

USER root

RUN npm config set proxy http://133.192.24.101:8080

RUN npm config set https-proxy http://133.192.24.101:8080

RUN npm i node-red-contrib-re-postgres --no-audit
